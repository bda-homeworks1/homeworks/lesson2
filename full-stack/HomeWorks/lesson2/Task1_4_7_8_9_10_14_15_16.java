package HomeWorks.lesson2;

import java.util.Scanner;

public class Task1_4_7_8_9_10_14_15_16{

    public static void main(String []args) {
        // task1();
        // task4();
        // task7();
        // task8();
        task9_10_14_15_16();
    }

    //TASK 1
    public static void task1() {
        System.out.println("sozu daxil edin:");
        Scanner sa = new Scanner(System.in);
        String a = sa.next();

        System.out.println("indeksi daxil edin:");
        Scanner sb = new Scanner(System.in);
        int b = sb.nextInt();

        System.out.println("indeks " + b + "-de olan character - " + a.charAt(b));
    }
    //TASK 4
    public static void task4() {
        System.out.println("stringi daxil edin:");
        Scanner sa = new Scanner(System.in);
        String a = sa.next();
        String b = "";

        for(int i=0;i<a.length();i++){
            if(a.charAt(i) == ','){
                b = b + a.charAt(i) + " ";
            } else{
                b = b + a.charAt(i);
            }
        }
        System.out.println(b);
    }
    //TASK 7
    public static void task7() {
        System.out.println("daxil edin:");
        Scanner sa = new Scanner(System.in);
        int a = sa.nextInt();
        double b = (double) a;
        System.out.println(b);
    }
    //TASK 8
    public static void task8() {
        System.out.println("a daxil edin:");
        Scanner sa = new Scanner(System.in);
        double a = sa.nextDouble();

        System.out.println("b daxil edin:");
        Scanner sb = new Scanner(System.in);
        double b = sb.nextDouble();

        double c = a/b;
        System.out.println("netice: " + c);
    }
    //TASK 9-10-14-15-16
    public static void task9_10_14_15_16() {
        byte a = -100;
        int b = a;
        System.out.println("int: " + b);

        double c = (double) b;
        System.out.println("double: " + c);

        short d = 10;
        b = d;
        System.out.println("short to Int: " + b);

        String s = "45";
        b = Integer.parseInt(s);
        System.out.println("string to Int: " + b);
        System.out.println("string to Int + uzerine 15 gelin: " + (b+15));



    }




}
