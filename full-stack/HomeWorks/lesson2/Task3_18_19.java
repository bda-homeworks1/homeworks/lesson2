package HomeWorks.lesson2;

/* Online Java Compiler and Editor */
import java.util.Scanner;

public class Task3_18_19 {

    public static void main(String []args) {
        // task3();
        // task18();
        task19();
    }
    //TASK 3
    public static void task3() {
        System.out.println("daxil edin:");
        Scanner sa = new Scanner(System.in);
        String a = sa.next();

        for(int i=0; i<a.length(); i++) {
            if(Character.isDigit( a.charAt(i))) {
                int kv = Character.getNumericValue(a.charAt(i));
                System.out.println(kv * kv);
            }
        }
    }

    //TASK 18
    public static void task18() {
        System.out.println("daxil edin:");
        Scanner sa = new Scanner(System.in);
        String a = sa.next();


        for(int i=0; i<a.length(); i++ ) {
            if (Character.isLowerCase(a.charAt(i))) {
                System.out.print(Character.toUpperCase(a.charAt(i)));
            } else {
                System.out.print(Character.toLowerCase(a.charAt(i)));
            }
        }

    }
    //TASK 19
    public static void task19() {
        System.out.println("daxil edin:");
        Scanner sa = new Scanner(System.in);
        String a = sa.next();
        String b = a.trim();

        System.out.println(b);

    }

}
